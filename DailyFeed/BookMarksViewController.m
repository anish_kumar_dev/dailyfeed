//
//  BookMarksViewController.m
//  DailyFeed
//
//  Created by Balwant on 31/10/15.
//  Copyright (c) 2015 Balwant. All rights reserved.
//

#import "BookMarksViewController.h"
#import "ArticleResponse.h"

@interface BookMarksViewController ()

@end

@implementation BookMarksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [ArticleResponse getArticlesResponseCompletion:^(ArticleResponse *response, NSError *error) {
        self.articals = response.articles;
        [self.tableView reloadData];
    }];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateTitle" object:@[@"Bookmarks",[UIImage imageNamed:@"book"]]];
}


#pragma mark - Tableview DataSource




@end
