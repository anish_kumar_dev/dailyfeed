//
//  ArticleCell.h
//  DailyFeed
//
//  Created by Balwant on 31/10/15.
//  Copyright (c) 2015 Balwant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *articleImage;
@property (weak, nonatomic) IBOutlet UILabel *articleTitle;




@end
