//
//  ArticleResponse.h
//  DailyFeed
//
//  Created by Balwant on 31/10/15.
//  Copyright (c) 2015 Balwant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArticleResponse : NSObject

@property (nonatomic, copy) NSArray *articles;

+ (void)getArticlesResponseCompletion:(void (^)(ArticleResponse *response, NSError *error))completion;

@end
