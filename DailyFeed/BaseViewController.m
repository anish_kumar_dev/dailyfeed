//
//  BaseViewController.m
//  DailyFeed
//
//  Created by Balwant on 31/10/15.
//  Copyright (c) 2015 Balwant. All rights reserved.
//

#import "BaseViewController.h"
#import "Article.h"
#import "ArticleCell.h"
#import "UIImageView+AFNetworking.h"
#import "ArticleDetailViewController.h"

@interface BaseViewController ()<UITableViewDataSource, UITableViewDelegate>


@end
@implementation BaseViewController

#pragma mark - Tableview Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark - Tableview DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.articals.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    
    Article *article = self.articals[indexPath.row];
    cell.articleTitle.text = article.title;
    [cell.articleImage setImageWithURL:[NSURL URLWithString:article.image]];
    
    return cell;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowArticle"]) {
        ArticleDetailViewController *dest = segue.destinationViewController;
        dest.article = self.articals[[self.tableView indexPathForSelectedRow].row];
    }
}

- (IBAction)backAction:(UIStoryboardSegue *)sender {
    
}


@end
