//
//  ArticleDetailViewController.h
//  DailyFeed
//
//  Created by Balwant on 31/10/15.
//  Copyright (c) 2015 Balwant. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Article;
@interface ArticleDetailViewController : UIViewController
@property (nonatomic, weak) Article *article;
@end
