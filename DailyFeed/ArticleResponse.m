//
//  ArticleResponse.m
//  DailyFeed
//
//  Created by Balwant on 31/10/15.
//  Copyright (c) 2015 Balwant. All rights reserved.
//

#import "ArticleResponse.h"
#import "ApiClient.h"
#import "Article.h"

@implementation ArticleResponse

+(void)getArticlesResponseCompletion:(void (^)(ArticleResponse *, NSError *))completion {
    [ApiClient getRequestURLWithPath:@"https://dailyhunt.0x10.info/api/dailyhunt?type=json&query=list_news" Params:nil Completion:^(id data, NSError *error) {
        if (data) {
            NSMutableArray *arr = [NSMutableArray new];
            for (id article in data[@"articles"]) {
                [arr addObject:[Article modalObjectWithDict:article]];
            }
            ArticleResponse *response = [ArticleResponse new];
            response.articles = arr;
            completion(response, nil);
        }
        else {
            completion(nil,nil);
        }
    }];
}

@end
