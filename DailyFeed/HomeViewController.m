//
//  HomeViewController.m
//  DailyFeed
//
//  Created by Balwant on 31/10/15.
//  Copyright (c) 2015 Balwant. All rights reserved.
//

#import "HomeViewController.h"
#import "ArticleResponse.h"
#import "ApiClient.h"
#import "MBProgressHUD.h"
#import "Article.h"

@interface HomeViewController ()<UISearchBarDelegate>
{
    NSMutableDictionary *filterDict;
}
@property (weak, nonatomic) IBOutlet UILabel *apiHitsLabel;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    filterDict = [NSMutableDictionary new];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [ArticleResponse getArticlesResponseCompletion:^(ArticleResponse *response, NSError *error) {
        self.articals = response.articles;
        [self.tableView reloadData];
        
        
        [self.articals enumerateObjectsUsingBlock:^(Article *obj, NSUInteger idx, BOOL *stop) {
            if (filterDict[obj.category]) {
                NSMutableArray *arr = filterDict[obj.category];
                [arr addObject:obj];
            }
            else {
                [filterDict setObject:[NSMutableArray new] forKey:obj.category];
            }
        }];
        
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    [ApiClient getApiHitsWithCompletion:^(id data, NSError *error) {
        self.apiHitsLabel.text = data[@"api_hits"];
    }];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateTitle" object:@[@"Home", [UIImage imageNamed:@"house"]]];
}

#pragma mark - Search delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
}


@end
