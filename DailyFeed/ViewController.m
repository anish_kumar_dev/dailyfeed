//
//  ViewController.m
//  DailyFeed
//
//  Created by Balwant on 31/10/15.
//  Copyright (c) 2015 Balwant. All rights reserved.
//

#import "ViewController.h"
#import "ArticleResponse.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *pageTitleButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"Hello : %@",[NSOperation new]);
    [ArticleResponse getArticlesResponseCompletion:^(ArticleResponse *response, NSError *error) {
        
    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTitle:) name:@"UpdateTitle" object:nil];
    
    [UIView appearance].layer.borderColor = [UIColor grayColor].CGColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}



#pragma Mark - 
- (void)updateTitle:(NSNotification *)notif {
    [self.pageTitleButton setTitle:[notif.object firstObject] forState:UIControlStateNormal];
    [self.pageTitleButton setImage:[notif.object lastObject] forState:UIControlStateNormal];
}


#pragma Mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"EembedContainer"]) {
        self.navCtrl = segue.destinationViewController;
    }
}


@end
