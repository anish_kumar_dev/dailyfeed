//
//  NewsArticle.m
//  
//
//  Created by Balwant on 31/10/15.
//
//

#import "NewsArticle.h"


@implementation NewsArticle

@dynamic image;
@dynamic category;
@dynamic source1;
@dynamic title1;
@dynamic content;
@dynamic url;

@end
