//
//  CustomView.m
//  DailyFeed
//
//  Created by Balwant on 31/10/15.
//  Copyright (c) 2015 Balwant. All rights reserved.
//

#import "CustomView.h"

@implementation CustomView
-(void)awakeFromNib {
    self.layer.borderColor = [UIColor colorWithRed:179/255.0 green:179/255.0 blue:179/255.0 alpha:1.0].CGColor;
}
@end
