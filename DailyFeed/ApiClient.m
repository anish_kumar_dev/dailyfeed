//
//  ApiClient.m
//  DailyFeed
//
//  Created by Balwant on 31/10/15.
//  Copyright (c) 2015 Balwant. All rights reserved.
//

#import "ApiClient.h"
#import "AFNetworking.h"

@implementation ApiClient

+(void)getRequestURLWithPath:(NSString *)path Params:(NSDictionary *)params Completion:(void (^)(id, NSError *))completion {
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:path parameters:nil success:^(AFHTTPRequestOperation * op, id resObj) {
        completion(resObj,nil);
    } failure:^(AFHTTPRequestOperation * op, NSError * error) {
        completion(nil,error);
    }];

}

+ (void)getApiHitsWithCompletion:(void (^)(id, NSError *))completion{
    
    NSString *path = @"https://dailyhunt.0x10.info/api/dailyhunt?type=json&query=api_hits";
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:path parameters:nil success:^(AFHTTPRequestOperation * op, id resObj) {
        completion(resObj,nil);
    } failure:^(AFHTTPRequestOperation * op, NSError * error) {
        completion(nil,error);
    }];
}


@end
