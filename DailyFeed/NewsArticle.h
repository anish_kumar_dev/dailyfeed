//
//  NewsArticle.h
//  
//
//  Created by Balwant on 31/10/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface NewsArticle : NSManagedObject

@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * source1;
@property (nonatomic, retain) NSString * title1;
@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * url;

@end
