//
//  NSDictionary+AKJSONString.h
//  DailyFeed
//
//  Created by Balwant on 31/10/15.
//  Copyright (c) 2015 Balwant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (AKJSONString)
-(NSString*)jsonStringWithPrettyPrint:(BOOL) prettyPrint;
@end
