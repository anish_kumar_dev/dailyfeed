//
//  BaseViewController.h
//  DailyFeed
//
//  Created by Balwant on 31/10/15.
//  Copyright (c) 2015 Balwant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController
@property (nonatomic, strong) NSArray *articals;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
