//
//  ApiClient.h
//  DailyFeed
//
//  Created by Balwant on 31/10/15.
//  Copyright (c) 2015 Balwant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApiClient : NSObject
+ (void)getRequestURLWithPath:(NSString *)path Params:(NSDictionary *)params Completion:(void (^)(id data, NSError *error))completion;

+ (void)getApiHitsWithCompletion:(void (^)(id data, NSError *error))completion;


@end
