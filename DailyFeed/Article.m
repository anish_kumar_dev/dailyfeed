//
//  Article.m
//  DailyFeed
//
//  Created by Balwant on 31/10/15.
//  Copyright (c) 2015 Balwant. All rights reserved.
//

#import "Article.h"

@implementation Article

#pragma mark - Factory
+ (Article *)modalObjectWithDict:(NSDictionary *)dict {
    return [[Article alloc] initWithDictionary:dict];
}

#pragma mark - INstance
- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        _title = dict[@"title"];
        _source = dict[@"source"];
        _category = dict[@"category"];
        _image = dict[@"image"];
        _content = dict[@"content"];
        _url = dict[@"url"];
    }
    return self;
}

@end
