//
//  ArticleDetailViewController.m
//  DailyFeed
//
//  Created by Balwant on 31/10/15.
//  Copyright (c) 2015 Balwant. All rights reserved.
//

#import "ArticleDetailViewController.h"
#import "UIImageView+AFNetworking.h"
#import "Article.h"

@interface ArticleDetailViewController ()
{
    CGFloat articleImageHeight;
}
@property (weak, nonatomic) IBOutlet UIImageView *articleImageView;
@property (weak, nonatomic) IBOutlet UITextView *articleTextView;
@property (weak, nonatomic) IBOutlet UILabel *sourceTitle;
@property (weak, nonatomic) IBOutlet UILabel *sourceLink;


@property (weak, nonatomic) IBOutlet UIButton *hideButton;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeightConstraint;

@end

@implementation ArticleDetailViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateTitle" object:@[@"Read Mode", [UIImage imageNamed:@"open-book1"]]];
    [self.articleImageView setImageWithURL:[NSURL URLWithString:self.article.image]];
    self.articleTextView.text = self.article.content;
    self.sourceTitle.text = self.article.source;
    self.sourceLink.text = self.article.url;
}

#pragma mark - IBActions
- (IBAction)hideImageAction:(id)sender {
    if (self.imageViewHeightConstraint.constant) {
        articleImageHeight = self.imageViewHeightConstraint.constant;
        self.imageViewHeightConstraint.constant = 0;
        self.viewHeightConstraint.constant -= articleImageHeight;
        [self.hideButton setImage:[UIImage imageNamed:@"down13"] forState:UIControlStateNormal];
        [UIView animateWithDuration:0.3 animations:^{
            [self.articleImageView.superview layoutSubviews];
            [self.view layoutSubviews];
        }];
    }
    else {
        self.imageViewHeightConstraint.constant = articleImageHeight;
        self.viewHeightConstraint.constant += articleImageHeight;
        [self.hideButton setImage:[UIImage imageNamed:@"up21"] forState:UIControlStateNormal];
        [UIView animateWithDuration:0.3 animations:^{
            [self.articleImageView.superview layoutSubviews];
            [self.view layoutSubviews];
        }];
    }
    
}
- (IBAction)scrollArticleAction:(id)sender {
}
- (IBAction)openLink:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.article.url]];
}
- (IBAction)shareAction:(id)sender {
    NSString *textToShare = self.article.title;
    NSURL *myWebsite = [NSURL URLWithString:self.article.url];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}


@end
